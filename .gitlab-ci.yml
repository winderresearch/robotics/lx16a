variables:
  PIP_CACHE_DIR: "${CI_PROJECT_DIR}/.cache/pip"

cache:
  key: "${CI_JOB_NAME}"
  paths:
    - .cache/pip
    - .venv

stages:
  - quality
  - tests
  - deploy

# Jobs templates ------------------

.install-deps-template: &install-deps
  before_script:
    - pip install poetry
    - poetry --version
    - poetry config virtualenvs.in-project true
    - poetry install -vv

.quality-template: &quality
  <<: *install-deps
  image: python:3.8
  stage: quality

.test-template: &test
  <<: *install-deps
  stage: tests
  coverage: '/TOTAL.*\s(\d+\.\d+\%)/'
  script: poetry run pytest -s --cov
  artifacts:
    paths:
      - tests/logs
    when: always
    expire_in: 1 week

.deploy-template: &deploy
  <<: *install-deps
  stage: deploy
  image: python:3.8

# Quality ----------------------

check-bandit:
  <<: *quality
  script: poetry run bandit -ll -r lx16a tests

check-black:
  <<: *quality
  script: poetry run black lx16a tests

check-safety:
  <<: *quality
  script: poetry run safety check

# Test ------------------------

python3.6:
  <<: *test
  image: python:3.6

python3.7:
  <<: *test
  image: python:3.7

python3.8:
  <<: *test
  image: python:3.8

coverage:
  <<: *install-deps
  image: python:3.8
  stage: tests
  variables:
    CODECOV_TOKEN: $CODECOV_TOKEN
  script: poetry run pytest -s --cov && poetry run codecov

build_test:
  <<: *install-deps
  image: python:3.8
  stage: tests
  script:
    - poetry build
  
# Publish -----------------------------

deploy_test:
  <<: *deploy
  variables:
    USERNAME: $TEST_USERNAME
    PASSWORD: $TEST_PASSWORD
  script:
    - poetry version $CI_JOB_ID
    - poetry config repositories.testpypi https://test.pypi.org/legacy/
    - poetry publish --build --repository testpypi --username $USERNAME --password $PASSWORD
    - sleep 10
    - pip install --extra-index-url https://pypi.org/simple -i https://test.pypi.org/simple/ $CI_PROJECT_NAME==$CI_JOB_ID
  except:
    - tags

deploy_production:
  <<: *deploy
  variables:
    USERNAME: $PRODUCTION_USERNAME
    PASSWORD: $PRODUCTION_PASSWORD
  script:
    - poetry version $CI_COMMIT_TAG
    - poetry publish --build --username $USERNAME --password $PASSWORD
  only:
    - tags
  